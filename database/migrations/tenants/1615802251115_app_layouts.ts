import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AppLayouts extends BaseSchema {
	protected tableName = 'app_layouts'

	public async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.increments('id')
			table.string('type').notNullable()
			table.string('name').notNullable()
			table.string('bg_color').nullable()
			table.string('color').nullable()
			table.string('pattern').notNullable()
			table.json('theme')
			table.json('sidebar')
			table.json('topbar')
			table.json('footer')
			table.json('main_area')
			table.timestamps(true)
		})
	}

	public async down() {
		this.schema.dropTable(this.tableName)
	}
}
