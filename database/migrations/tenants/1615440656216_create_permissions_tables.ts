import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Permissions extends BaseSchema {
  protected tableName = 'permissions'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').unsigned()
      table.integer('module_id').unsigned().nullable()
      table.string('slug').notNullable().unique()
      table.string('name').notNullable().unique()
      table.text('description').nullable()
      table.boolean('system_permission').defaultTo(false)
      table.timestamps(true)
      table.foreign('module_id').references('id').inTable('modules').onDelete('set null')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
