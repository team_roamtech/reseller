import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UserRole extends BaseSchema {
	protected tableName = 'user_role'

	public async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.increments('id')
			table.integer('role_id').unsigned().index()
			table.integer('user_id').unsigned().index()
			table.foreign('role_id')
				.references('id')
				.inTable('roles')
				.onDelete('cascade')
			table.foreign('user_id')
				.references('id')
				.inTable('users')
				.onDelete('cascade')
			table.timestamps(true)
		})
	}

	public async down() {
		this.schema.dropTable(this.tableName)
	}
}
