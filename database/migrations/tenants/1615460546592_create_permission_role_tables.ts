import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PermissionRole extends BaseSchema {
  protected tableName = 'permission_role'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('permission_id').unsigned().index()
      table.integer('role_id').unsigned().index()
      table.foreign('role_id')
        .references('id')
        .inTable('roles')
        .onDelete('cascade')
      table.foreign('permission_id')
        .references('id')
        .inTable('permissions')
        .onDelete('cascade')
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
