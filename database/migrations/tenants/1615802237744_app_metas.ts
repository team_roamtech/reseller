import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AppMetas extends BaseSchema {

	protected tableName = 'app_metas';

	public async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.increments('id')
			table.string('title').notNullable()
			table.string('description').nullable()
			table.string('site_name').nullable()
			table.string('type').nullable()
			table.string('url').nullable()
			table.string('site_image').nullable()
			table.timestamps(true)
		})
	}

	public async down() {
		this.schema.dropTable(this.tableName)
	}
}
