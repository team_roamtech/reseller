import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Users extends BaseSchema {

	protected tableName = 'users'

	public async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.increments('id').unsigned()
			table.string('name').nullable()
			table.string('email')
			table.string('password')
			table.string('phone_number').nullable()
			table.string('remember_me_token').nullable()
			table.boolean('is_verified').defaultTo(false)
			table.boolean('is_superuser').defaultTo(false)
			table.integer('company_id').nullable()
			table.timestamps(true)

			table.foreign('company_id')
				.references('id')
				.inTable('companies')
				.onDelete('cascade')
		})
	}

	public async down() {
		this.schema.dropTable(this.tableName)
	}
}
