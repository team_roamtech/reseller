import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AppMedias extends BaseSchema {

	protected tableName = 'app_medias'

	public async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.increments('id')
			table.string('favicon')
			table.string('logo_max')
			table.string('logo_mini')
			table.timestamps(true)
		})
	}

	public async down() {
		this.schema.dropTable(this.tableName)
	}
}
