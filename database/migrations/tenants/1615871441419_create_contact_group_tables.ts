import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CreateContactGroupTables extends BaseSchema {
  protected tableName = 'contact_groups'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name')
      table.integer('contact_row_count').defaultTo(0)
      table.text('headers').nullable
      table.string('file_path')
      table.string('file_name')
      table.string('collection_name').nullable()
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
