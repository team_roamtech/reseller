import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Users extends BaseSchema {
  protected tableName = 'users'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name')
      table.string('email')
      table.string('phone_number').nullable()
      table.string('password')
      table.boolean('is_verified').defaultTo(false)
      table.integer('tenant_id')
      table.timestamps(true)

      table.foreign('tenant_id').references('id').inTable('tenants').onDelete('cascade')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
