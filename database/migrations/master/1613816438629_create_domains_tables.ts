import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Domains extends BaseSchema {
  protected tableName = 'domains'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('domain').unique()
      table.string('fqdn').unique()
      table.integer('tenant_id')
      table.string('redirect_to').nullable()
      table.boolean('force_https').defaultTo(false)
      table.timestamps(true)
      table
        .foreign('tenant_id')
        .references('id')
        .inTable('tenants')
        .onDelete('cascade')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
