import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Tenants extends BaseSchema {
	protected tableName = 'tenants'

	public async up() {
		this.schema.createTable(this.tableName, (table) => {
			table.increments('id')
			table.uuid('uuid').unique()
			table.string('name').notNullable()
			table.boolean('active').defaultTo(false)
			table.string('status').defaultTo('provisioning')
			table.json('config').nullable()
			table.json('data').nullable()
			table.timestamps(true)
		})
	}

	public async down() {
		this.schema.dropTableIfExists(this.tableName)
	}
}
