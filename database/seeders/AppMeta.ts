import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import AppMeta from "App/Models/Tenants/AppMeta";

export default class AppMetaSeeder extends BaseSeeder {

	public async run() {

		console.log('APP META SEEDER : started.................................');

		await AppMeta.create({
			title: 'Bulk-SMS',
			description: 'This is a bulks sms portal',
			site_name: 'Bulk SMS',
			type: 'testtype',
			url: 'testurl',
			site_image: 'testimage'
		})

		console.log('APP META SEEDER : ended.................................');
	}
}
