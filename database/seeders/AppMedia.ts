import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import AppMedia from "App/Models/Tenants/AppMedia";

export default class AppMediaSeeder extends BaseSeeder {

	public async run() {

		console.log('APP MEDIA SEEDER : started.................................');

		await AppMedia.create({
			favicon: 'testlink',
			logo_max: 'testlink',
			logo_mini: 'testlink'
		});

		console.log('APP MEDIA SEEDER : ended.................................');
	}
}
