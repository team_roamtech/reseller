import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import AppLayout from "App/Models/Tenants/AppLayout";

export default class AppLayoutSeeder extends BaseSeeder {

	public async run() {

		console.log('APP LAYOUT SEEDER : started.................................');

		await AppLayout.createMany([
			{
				type: 'guest',
				name: 'guest_1',
				pattern: 'ltr',
				bg_color: '',
				color: '',
				theme: JSON.stringify(await this.sidebarData()),
				sidebar: JSON.stringify(await this.sidebarData()),
				topbar: JSON.stringify(await this.topbarData()),
				footer: JSON.stringify(await this.footerData()),
				main_area: JSON.stringify(await this.mainAreaData())
			},
			{
				type: 'main',
				name: 'parent_1',
				pattern: 'ltr',
				bg_color: '',
				color: '',
				theme: JSON.stringify(await this.sidebarData()),
				sidebar: JSON.stringify(await this.sidebarData()),
				topbar: JSON.stringify(await this.topbarData()),
				footer: JSON.stringify(await this.footerData()),
				main_area: JSON.stringify(await this.mainAreaData())
			}
		])

		console.log('APP LAYOUT SEEDER : ended.................................');
	}

	async sidebarData()
	{
		return {
			show: false,
			name: 'dark-purple',
			component: 'sidebar_1',
			bg_color: 'bg-purple-800 bg-opacity-100',
			color: 'text-white',
			font: '',
			weight: 'normal',
			shadow: '',
			active_menu_item: {
				bg_color: 'bg-purple-900',
				hover: 'hover:bg-purple-900',
				color: 'text-white',
			},
		}
	}

	async topbarData()
	{
		return {
			show: true,
			name: 'dark-purple',
			component: 'topbar_guest_1',
			bg_color: 'bg-purple-800 bg-opacity-100',
			color: 'text-white',
			font: '',
			weight: 'normal',
			shadow: '',
			active_menu_item: {
				bg_color: 'bg-purple-900',
				hover: 'hover:bg-purple-900',
				color: 'text-white',
			},
		}
	}

	async footerData()
	{
		return {
			show: true,
			terms_and_conditions_url: ''
		}
	}

	async themeData()
	{
		return {
			label: 'light',
			bg_color: '',
			btn_color: '',
		}
	}

	async mainAreaData()
	{
		return {
			bg_color: '',
			color: ''
		}
	}
}
