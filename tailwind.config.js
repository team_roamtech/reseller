module.exports = {
    purge: [],
    darkMode: false,
    theme: {
        extend: {
            width: {
                '1/8': '12.5%',
                '2/8': '25.0%',
                '3/8': '37.5%',
                '4/8': '50.0%',
                '5/8': '62.5%',
                '6/8': '75.0%',
                '7/8': '87.5%',
            }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
