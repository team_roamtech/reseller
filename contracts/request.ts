declare module '@ioc:Adonis/Core/Request' {
  import Tenant from 'App/Models/Landlord/Tenant'

  interface RequestContract {
    tenant: Tenant
  }
}
