import Migrate from "@adonisjs/lucid/build/commands/Migration/Run";
import {args} from "@adonisjs/ace";
import Tenant from "App/Models/Landlord/Tenant";
import {ConnectionResolver} from "App/Sevices/ConnectionResolver";
import {parse} from 'uuid'

export default class TenantMigrate extends Migrate {

    /**
     * Command Name is used to run the command
     */
    public static commandName = 'tenant:migrate:run'

    /**
     * Command Name is displayed in the "help" output
     */
    public static description = 'Run pending tenant migrations'

    /**
     * Choose a custom pre-defined connection. Otherwise, we use the
     * default connection
     */
    @args.string({description: 'Define a tenant uuid to be used to reslove the database connection'})
    public uuid: string

    public async run() {
        if (this.uuid === 'all') {
            const tenants = await Tenant.all()
            for (let tenant of tenants) {
                await this.migrate(tenant)
            }
            return
        }
        try {
            parse(this.uuid)
        } catch (e) {
            this.logger.error(
                `"${this.uuid}" is not a valid uuid`,
            )
            return
        }

        try {
            const tenant = await Tenant.findByOrFail('uuid', this.uuid)
            await this.migrate(tenant)
        } catch (e) {
            this.logger.error(
                `"${this.uuid}" is not a valid tenant uuid.`,
            )
        }

    }

    private async migrate(tenant: Tenant) {
        this.connection = tenant.connection
        ConnectionResolver.resolve(tenant)
        await super.run()
    }
}
