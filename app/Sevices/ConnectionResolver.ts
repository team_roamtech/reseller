import Config from '@ioc:Adonis/Core/Config'
import Database from '@ioc:Adonis/Lucid/Database'
import Domain from 'App/Models/Landlord/Domain'
import Tenant from 'App/Models/Landlord/Tenant'
import path from 'path'

export class ConnectionResolver {
	public static async resolveByHostname(hostname: string): Promise<Tenant | boolean> {
		const domain = await Domain.findBy('fqdn', hostname)
		if (null === domain) {
			return false
		}
		await domain.load('tenant')
		return ConnectionResolver.resolve(domain.tenant)
	}

	public static async resolveByUUID(uuid: string | undefined | null): Promise<Tenant | boolean> {
		if (!uuid) {
			return false
		}
		const tenant = await Tenant.query().where('uuid', uuid).first()
		if (null === tenant) {
			return false
		}
		return ConnectionResolver.resolve(tenant)
	}

	public static resolve(tenant: Tenant): Tenant {
		const connection = Config.get('database.connection')
		let defaultConfig = Config.get(`database.connections.${connection}.connection`)
		const tenantConnectionConfig = {
			client: connection,
			connection: {
				...defaultConfig,
				...tenant.config.database,
			},
			migrations: {
				paths: [path.resolve(__dirname + '/../../database/migrations/tenants')],
			},
		}

		if (!Database.manager.has(tenant.connection)) {
			Database.manager.add(tenant.connection, tenantConnectionConfig)
			Database.manager.connect(tenant.connection)
		}

		return tenant
	}
}
