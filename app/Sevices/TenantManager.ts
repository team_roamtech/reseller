import Database from '@ioc:Adonis/Lucid/Database'
import Tenant from 'App/Models/Landlord/Tenant'
import {Migrator} from '@adonisjs/lucid/build/src/Migrator'
import {ConnectionResolver} from 'App/Sevices/ConnectionResolver'
import Application from '@ioc:Adonis/Core/Application'
import Logger from '@ioc:Adonis/Core/Logger'
import {SeedsRunner} from "@adonisjs/lucid/build/src/SeedsRunner";
import {metaData} from "Config/layouts";
import { MetaDataRepository } from 'App/Repositories/Settings/MetaDataRepository';
import {AppMediaRepository} from "App/Repositories/Settings/AppMediaRepository";
import {LayoutRepository} from "App/Repositories/Settings/LayoutRepository";

export class TenantManager {
	constructor(protected tenant: Tenant) {
	}

	private async createDatabase() {
		await Database.knexQuery().client.raw(`CREATE DATABASE ${this.tenant.config.database.database}`)
	}

	private async createMongoDb()
	{
		
	}

	private async registerConnection() {
		ConnectionResolver.resolve(this.tenant)
	}

	private async runMigrations() {
		const connection = Database.getRawConnection(this.tenant.connection)
		if (null === connection) {
			Logger.warn('Cannot resolve connection for tenant => ', {tenant: this.tenant})
			return
		}
		const migrator = new Migrator(Database, Application, {
			direction: 'up',
			connectionName: this.tenant.connection,
			dryRun: false,
		})
		migrator.on('migration:start', (event) => {
			Logger.info({event, tenant: this.tenant}, 'Migration start for tenant')
		})
		migrator.on('migration:completed', (event) => {
			Logger.info({event, tenant: this.tenant}, 'Migration Complete for tenant')
		})
		migrator.on('migration:error', (event) => {
			Logger.error({event, tenant: this.tenant}, 'An error was encountered while running migrations')
		})
		await migrator.run()
	}

	public async syncSettingsData()
	{
		await (new MetaDataRepository()).store(metaData.meta, this.tenant.connection);
		await (new AppMediaRepository()).store(metaData.media, this.tenant.connection);

		console.log(metaData.layouts);

		metaData.layouts.forEach( async (layout) => {
			await (new LayoutRepository()).store(layout, this.tenant.connection);
		})
	}

	public async seedDatabase()
	{
		let runner = new SeedsRunner(Database, Application, this.tenant.connection);

		const files = await runner.getList();

		let selectedFileNames: string[] = files.map(({ name }) => name);

		for (let fileName of selectedFileNames) {

			const sourceFile = files.find(({ name }) => fileName === name);

			if (!sourceFile) {
				console.log('SOURCE FILE NOT FOUND FOR ' +fileName);
				return;
			}

			await runner.run(sourceFile);

			Logger.info('Seeding Complete for tenant '+fileName);
		}
	}

	public async initialize() {
		await this.createDatabase()
		await this.createMongoDb()
		await this.registerConnection()
		await this.runMigrations()

		// await this.seedDatabase()
		await this.syncSettingsData()
	}
}
