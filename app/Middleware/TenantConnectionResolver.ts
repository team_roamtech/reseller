import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import {ConnectionResolver} from 'App/Sevices/ConnectionResolver'
import Tenant from 'App/Models/Landlord/Tenant'

export default class TenantConnectionResolver {
  public async handle (ctx: HttpContextContract, next: () => Promise<void>) {
    const hostname = ctx.request.hostname()?.toString().trim() || ''
    const uuid = ctx.request.header('x-account-id')
    const tenant = uuid ?
      await ConnectionResolver.resolveByUUID(uuid) :
      await ConnectionResolver.resolveByHostname(hostname)
    if(!(tenant instanceof Tenant)) {
      ctx.response.abort(
        `Cannot resolve a site for ${hostname}`,
      )
    }
    ctx.request.tenant = tenant

    await next()
  }
}
