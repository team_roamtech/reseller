import {DateTime} from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import {
    column,
    beforeSave,
    BaseModel,
    belongsTo,
    BelongsTo,
    manyToMany,
    ManyToMany
} from '@ioc:Adonis/Lucid/Orm'
import Company from 'App/Models/Tenants/Company'
import Role from "App/Models/Tenants/Role";

export default class User extends BaseModel {
	@column({isPrimary: true})
	public id: number

	@column()
	public name?: string

	@column()
	public email: string

	@column({serializeAs: null})
	public password: string

	@column({serializeAs: null})
	public rememberMeToken?: string

	@column()
	public phoneNumber?: string

	@column()
	public isVerified?: boolean

	@column()
	public isSuperuser: boolean

	@column()
	public companyId: number

	@column.dateTime({autoCreate: true})
	public createdAt: DateTime

	@column.dateTime({autoCreate: true, autoUpdate: true})
	public updatedAt: DateTime

	@belongsTo(() => Company)
	public company: BelongsTo<typeof Company>

    @manyToMany(() => Role, {
        pivotForeignKey: 'user_id',
        pivotRelatedForeignKey: 'role_id',
        pivotTable: 'role_user'
    })
    public roles: ManyToMany<typeof Role>

	@beforeSave()
	public static async hashPassword(user: User) {
		if (user.$dirty.password) {
			user.password = await Hash.make(user.password)
		}
	}
}
