import {DateTime} from 'luxon'
import {BaseModel, beforeCreate, column, hasMany, HasMany} from '@ioc:Adonis/Lucid/Orm'
import {v4 as uuid} from 'uuid'
import User from 'App/Models/Tenants/User'

export default class Company extends BaseModel {
	@column({isPrimary: true})
	public id: number

	@column()
	public name: string

	@column()
	public uuid: string

	@column()
	public website?: string

	@column()
	public primaryEmail?: string

	@column()
	public phoneNumber?: string

	@column.dateTime({autoCreate: true})
	public createdAt: DateTime

	@column.dateTime({autoCreate: true, autoUpdate: true})
	public updatedAt: DateTime

	@hasMany(() => User)
	public users: HasMany<typeof User>

	@beforeCreate()
	public static async addUUID(company: Company) {
		company.uuid = uuid().toString()
	}
}
