import {DateTime} from 'luxon'
import {BaseModel, beforeSave, BelongsTo, belongsTo, column} from '@ioc:Adonis/Lucid/Orm'
import slugify from "slugify";
import Module from "App/Models/Tenants/Module";

export default class Permission extends BaseModel {
    @column({isPrimary: true})
    public id: number

    @column()
    public name: string

    @column()
    public description?: string

    @column()
    public slug: string

    @column()
    public moduleId: number


    @column.dateTime({autoCreate: true})
    public createdAt: DateTime

    @column.dateTime({autoCreate: true, autoUpdate: true})
    public updatedAt: DateTime

    @belongsTo(() => Module)
    public module: BelongsTo<typeof Module>

    @beforeSave()
    public static async slugify(model: Permission) {
        if(model.$dirty.name) {
            model.slug = slugify(model.name, {lower: true})
        }
    }
}
