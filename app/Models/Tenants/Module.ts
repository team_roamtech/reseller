import {DateTime} from 'luxon'
import {BaseModel, column, HasMany, hasMany} from '@ioc:Adonis/Lucid/Orm'
import Permission from "App/Models/Tenants/Permission";

export default class Module extends BaseModel {
    @column({isPrimary: true})
    public id: number

    @column()
    public name: string

    @column.dateTime({autoCreate: true})
    public createdAt: DateTime

    @column.dateTime({autoCreate: true, autoUpdate: true})
    public updatedAt: DateTime

    @hasMany(() => Permission)
    public permissions: HasMany<typeof Permission>
}
