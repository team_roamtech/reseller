import {DateTime} from 'luxon'
import {BaseModel, belongsTo, column, BelongsTo} from '@ioc:Adonis/Lucid/Orm'
import Tenant from 'App/Models/Landlord/Tenant'

export default class Domain extends BaseModel {
  @column({isPrimary: true})
  public id: number

  @column()
  public fqdn: string

  @column()
  public domain: string

  @column()
  public redirectTo?: string | null

  @column()
  public forceHttps: boolean

  @column()
  public tenantId: number

  @column.dateTime({autoCreate: true})
  public createdAt: DateTime

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public updatedAt: DateTime

  @belongsTo(() => Tenant)
  public tenant: BelongsTo<typeof Tenant>
}
