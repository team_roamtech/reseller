import {DateTime} from 'luxon'
import {BaseModel, beforeCreate, column, hasMany, HasMany} from '@ioc:Adonis/Lucid/Orm'
import Domain from 'App/Models/Landlord/Domain'
import {TenantManager} from 'App/Sevices/TenantManager'

type TenantConfig = {
  database: {
    host?: string;
    user?: string;
    password?: string;
    database?: string;
    port?: number;
  }
}

export default class Tenant extends BaseModel {
  @column({isPrimary: true})
  public id: number

  @column()
  public uuid: string

  @column()
  public name: string

  @column()
  public status: string

  @column()
  public active: boolean

  @column()
  public config: TenantConfig

  @column()
  public data?: object

  @column.dateTime({autoCreate: true})
  public createdAt: DateTime

  @column.dateTime({autoCreate: true, autoUpdate: true})
  public updatedAt: DateTime

  @hasMany(() => Domain)
  public domains: HasMany<typeof Domain>

  @beforeCreate()
  public static async createDatabase (tenant: Tenant) {
    const tenantManager = new TenantManager(tenant)
    await tenantManager.initialize()
  }

  public get connection() {
    return `tenant_${this.uuid}`
  }
}
