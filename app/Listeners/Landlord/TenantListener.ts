import {EventsList} from '@ioc:Adonis/Core/Event'
import {ConnectionResolver} from 'App/Sevices/ConnectionResolver'
import User from 'App/Models/Tenants/User'
import Company from 'App/Models/Tenants/Company'

export default class TenantListener {
    /**
     * Creates a user after tenant has been created
     *
     * @param event
     */
    public async handleCreated (event: EventsList['new:tenant']) {
        ConnectionResolver.resolve(event.tenant)
        const values = {
            name: event.tenant.name,
            phoneNumber: event.data.phoneNumber,
        }
        const company = await Company.create(values, {connection: event.tenant.connection})
        const user = await User.create(event.data, {connection: event.tenant.connection})
        await user.related('company').associate(company)
    }
}
