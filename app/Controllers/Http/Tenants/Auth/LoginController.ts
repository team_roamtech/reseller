import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import {rules, schema} from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/Tenants/User'
import Hash from '@ioc:Adonis/Core/Hash'
import {AuthenticationException} from '@adonisjs/auth/build/standalone'

export default class LoginController {
	public async login({request, auth}: HttpContextContract) {
		const connection = request.tenant.connection
		const validator = this.validate(connection)
		const credentials = await request.validate({schema: validator})
		const user = await User.findByOrFail('email', credentials.email, {connection})
		if (await Hash.verify(user.password, credentials.password)) {
			return await auth.use('api').login(user, {
				expiresIn: '1 day',
			})
		}
		throw new AuthenticationException('User does not exist or invalid credentials', '401')
	}

	public async profile({auth}: HttpContextContract) {
		return auth.user
	}

	/**
	 * Create an instance of the validation schema
	 *
	 * @param connection
	 * @private
	 */
	private validate(connection: string) {
		return schema.create({
			password: schema.string({}, [rules.required()]),
			email: schema.string({escape: true}, [
				rules.email({sanitize: {lowerCase: true}}),
				rules.required(),
				rules.exists({
					table: 'users',
					column: 'email',
					connection,
				}),
			]),
		})
	}
}
