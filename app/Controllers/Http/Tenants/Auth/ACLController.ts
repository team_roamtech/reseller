import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import {rules, schema} from '@ioc:Adonis/Core/Validator'
import CreateRole from "App/Tasks/Auth/CreateRole";
import Permission from "App/Models/Tenants/Permission";

export default class ACLController {
    public async createRole({request}: HttpContextContract) {
        const validator = schema.create({
            name: schema.string({}, [rules.required()]),
            description: schema.string({trim: true}, [rules.maxLength(255)]),
        })

        const data = await request.validate({
            schema: validator,
        })
        return await CreateRole.run(request.tenant.connection, data)
    }

    public async createPermission({request}: HttpContextContract) {
        const data = await request.validate({
            schema: schema.create({
                name: schema.string({}, [
                    rules.unique({
                        table: 'permissions',
                        column: 'name',
                        connection: request.tenant.connection,
                        caseInsensitive: true,
                    }),
                ]),
                description: schema.string({trim: true, escape: true}, [rules.maxLength(255)]),
                moduleId: schema.number([
                    rules.exists({
                        table: 'modules', column: 'id', connection: request.tenant.connection,
                    }),
                ]),
            }),
        })

        return await Permission.create(data, {connection: request.tenant.connection})
    }

    public async getAllPermissions({request}: HttpContextContract) {
        return await Permission.all({connection: request.tenant.connection})
    }
}
