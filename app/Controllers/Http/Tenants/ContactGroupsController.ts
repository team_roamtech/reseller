// import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import ContactGroup from 'App/Models/Tenants/ContactGroup'
import * as fs from 'fs';
import * as path from 'path';
import * as csv from 'fast-csv';
import Config from '@ioc:Adonis/Core/Config'
import { MongoClient } from "mongodb";
import { v4 as uuid } from 'uuid'
import { networkMapper } from 'App/Helpers/utils';
import { Console } from 'node:console';
import { ConnectionResolver } from 'App/Sevices/ConnectionResolver';
import ObjectId from '@ioc:Mongodb/ObjectId';


export default class ContactGroupsController {

  public async store({ request, response }: HttpContextContract) {

    const tenant = request.tenant
    const name = request.input('name')
    let headerItems: any = []
   // var rowItems = new Array()
    const collectionName = uuid()
    const url = Config.get('mongodb.connections.mongodb.url');
   
    //mongodb connection
    const client = MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });

    //get the csv file
    const dataFile = request.file('contact_file', {
      extnames: ['csv', 'json'],
    })


    if (!dataFile) {
      return dataFile
    }

    if (dataFile.hasErrors) {
      return dataFile.errors
    }



    // save the file to a folder
    await dataFile.move(Application.tmpPath('uploads')) // 

    console.log('STARTED => ', new Date())
    //get the file from the folder and get its content
    var fileUploadPromise = () => {
      return new Promise(async(resolve, reject) => {
        fs.createReadStream(path.resolve(__dirname, Application.tmpPath('uploads'), dataFile.clientName))
          .pipe(csv.parse({ headers: true }))
          .on('error', error => console.error(error))
          .on('headers', function (d) {

            //save header information to a list
            for (var i = 0; i < d.length; i++) {
              let item: string = d[i];
              headerItems.push(item);
            }
            headerItems.push('network');

          })
          .on('data', async (row) => {

            row = await networkMapper(row, 'msisdn');

           // rowItems.push(row);

              //insert contacts into MongoDB
              await client.then(async (dbclient) => {
                var db = dbclient.db('contacts');
                db.collection(collectionName).insertOne(row, async function (error) {
                  if (error) {
                    console.error(error);
                    var errorMAp = {
                      message: 'Error',
                      data: error
                    };
                    reject(errorMAp);
                  } else {
                    //if successfull create contact group
                
                  }
                });
  
              });
          })
          .on('end', async (rowCount: number) => {
            const contactGroup = await ContactGroup.create({
              name, contactRowCount: rowCount, headers: headerItems, filePath: dataFile.filePath, fileName: dataFile.fileName, collectionName,
            }, { connection: tenant.connection });

            console.log("Successfully inserted row items");
            console.log('Ended! => ', new Date());
            var succesMap = {
              message: 'Succesful',
              type: 'Contacts Group',
              data: contactGroup
            };
            resolve(succesMap);
          });

      })
    }
    //await myPromise
    var result = await fileUploadPromise();
    //continue execution
    (await client).close;

    return response.json(result);
//  }
  }



  //Get contacts and contacts group
  public async retreive({ request, response }: HttpContextContract) {
    const connection = request.tenant.connection
    const id = request.param('id')
    const url = Config.get('mongodb.connections.mongodb.url');
    const dbitem = Config.get('mongodb.connections.mongodb.database');
    const client = MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
    const contactGroup = await ContactGroup.find(id, {connection})
    const collectionName:string = contactGroup?.collectionName as string

   await client.then(async(clienti) => {
      var db = clienti.db(dbitem)
          //Declare promise
     var myPromise = () => {
      return new Promise((resolve, reject) => {


        db.collection(collectionName).find()
         .toArray(function(err, data) {
            err 
               ? reject({
                message:'Error',
                type:'Contacts',
                data:err
              }) 
               : resolve({
                message:'Succesful',
                type:'Contacts',
                contactGroup:contactGroup,
                contacts:data
              });
          });
      });
    };
    //await myPromise
    var result = await myPromise();
    //continue execution
    (await client).close;

    //return response
    return response.json(result);

    })
  }

  //edit contact group
  public async editContactGroup({ request, response }: HttpContextContract) {
    //get values from request
    const connection = request.tenant.connection
    const id = request.param('id')
    const {name} = request.only(['name'])

    //save contacts group name
    const contactGroup = await ContactGroup.findOrFail(id, {connection})
    if(name!=null)contactGroup.name = name
    await contactGroup.save()

    return response.json({
              message:'Succesful',
              type:'Edit Contacts',
              data:contactGroup
            });
  }


  //delete contact group and contacts herein
  public async deleteContactGroup({ request, response }: HttpContextContract) {
    //get values from request
    const connection = request.tenant.connection
    const id = request.param('id')
    const url = Config.get('mongodb.connections.mongodb.url');
    const dbitem = Config.get('mongodb.connections.mongodb.database');
    const client = MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
    const contactGroup = await ContactGroup.findOrFail(id, {connection})
    const collectionName:string = contactGroup?.collectionName as string

   await client.then(async(clienti) => {
    var db = clienti.db(dbitem)
        //Declare promise
   var myPromise = () => {
    return new Promise(async(resolve, reject) => {

      //drop contacts from mongo db and contact group from prosgress db
    db.collection(collectionName).drop()
      .then(result => {
        contactGroup.delete()
        resolve({
          message:'Deleted',
          type:'Delete Contacts',
          data:result,
          contactGroup:contactGroup
        })
      }) 
      .catch(err => {
        reject({
          message:'Error',
          type:'Not deleted',
          error:err
        })
      })

    });
  };
  //await myPromise
  var result = await myPromise();
  //continue execution
  (await client).close;

  //return response
  return response.json(result);

  })
  }


//add one contact
public async addContact({ request, response }: HttpContextContract) {
  const connection = request.tenant.connection
 // console.log(request.body())
  const id = request.param('id')
  const url = Config.get('mongodb.connections.mongodb.url');
  const dbitem = Config.get('mongodb.connections.mongodb.database');
  const client = MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
  const contactGroup = await ContactGroup.find(id, {connection})
  const collectionName:string = contactGroup?.collectionName as string

 await client.then(async(clienti) => {
    var db = clienti.db(dbitem)
        //Declare promise
   var myPromise = () => {
    return new Promise((resolve, reject) => {


      db.collection(collectionName).insertOne(request.body())
      .then(result => {
          resolve({
            message:'Add',
            type:'Add Contacts',
            data:result,
            contactGroup:contactGroup
          })
        
      })
      .catch(err => {
        console.error(`Failed to add the item: ${err}`)
        reject({
          message:'Error',
          type:'Add Contact',
          data:err
        }) 
      })
    });
  };
  //await myPromise
  var result = await myPromise();
  //continue execution
  (await client).close;
  //return response
  return response.json(result);
  })
}

  //edit one contact
  public async editContact({ request, response }: HttpContextContract) {
    const connection = request.tenant.connection
   // console.log(request.body())
    const id = request.param('id')
    const _id = request.param('_id')
    const url = Config.get('mongodb.connections.mongodb.url');
    const dbitem = Config.get('mongodb.connections.mongodb.database');
    const client = MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
    const contactGroup = await ContactGroup.find(id, {connection})
    const collectionName:string = contactGroup?.collectionName as string

   await client.then(async(clienti) => {
      var db = clienti.db(dbitem)
          //Declare promise
     var myPromise = () => {
      return new Promise((resolve, reject) => {

        const query =  {"_id": new ObjectId(_id)};

        const update = {
           $set: request.body()
        };

        const options = { upsert: true, multi: true };

        db.collection(collectionName).updateOne(query, update, options)
        .then(result => {
          const { matchedCount, modifiedCount } = result;
          if(matchedCount && modifiedCount) {
            console.log(`Successfully updated the item.`)
            resolve({
              message:'Edited/Updated',
              type:'Edit Contacts',
              data:result,
              contactGroup:contactGroup
            })
          }else{
            reject({
              message:'Contacts does not exist',
              type:'Edit Contacts'
            })
           
          }
        })
        .catch(err => {
          console.error(`Failed to update the item: ${err}`)
          reject({
            message:'Error',
            type:'Contacts',
            data:err
          }) 
        })
      });
    };
    //await myPromise
    var result = await myPromise();
    //continue execution
    (await client).close;
    //return response
    return response.json(result);
    })
  }


  //delete one contact
  public async deleteContact({ request, response }: HttpContextContract) {
    const connection = request.tenant.connection
    console.log(request.body())
    const id = request.param('id')
    const _id = request.param('_id')
    const url = Config.get('mongodb.connections.mongodb.url');
    const dbitem = Config.get('mongodb.connections.mongodb.database');
    const client = MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
    const contactGroup = await ContactGroup.find(id, {connection})
    const collectionName:string = contactGroup?.collectionName as string

   await client.then(async(clienti) => {
      var db = clienti.db(dbitem)
          //Declare promise
     var myPromise = () => {
      return new Promise((resolve, reject) => {

        const query =  {"_id": new ObjectId(_id)};

        db.collection(collectionName).deleteOne(query)
        .then(result => {
          console.log(result)
         if(result.deletedCount){
          resolve({
            message:'Delete',
            type:'Delete Contacts',
            data:result,
            contactGroup:contactGroup
          })
         }else{
          reject({
            message:'Contact does not exist',
            type:'Delete'
          })
         }
        })
        .catch(err => {
          console.error(`Failed to Delete the item: ${err}`)
          reject({
            message:'Error',
            type:'Delete',
            data:err
          }) 
        })
      });
    };
    //await myPromise
    var result = await myPromise();
    //continue execution
    (await client).close;
    //return response
    return response.json(result);
    })
  }

}
