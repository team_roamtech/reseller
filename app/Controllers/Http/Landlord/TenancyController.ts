import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import {tenantSignUpSchema} from 'App/Schemas/tenant-signup'
import Tenant from 'App/Models/Landlord/Tenant'
import {v4 as uuid} from 'uuid'
import Domain from 'App/Models/Landlord/Domain'
import Env from '@ioc:Adonis/Core/Env'
import {randomString} from 'App/Helpers/utils'
import Event from '@ioc:Adonis/Core/Event'

export default class TenancyController {
	/**
	 * Register a new tenant and bootstrap tenancy configs, data and the database
	 *
	 * @param ctx
	 */
	public async register({request}: HttpContextContract) {
		const validated = await request.validate({schema: tenantSignUpSchema})
		const tenant = await Tenant.create({
			name: validated.domain,
			uuid: uuid(),
			status: 'created',
			active: false,
			config: {
				database: {
					database: `tenant_${uuid().toString().replace(/-/g, '')}`,
				},
			},
		})
		const domain = new Domain()
		domain.fqdn = `${validated.domain}.${Env.get('BASE_DOMAIN')}`
		domain.domain = validated.domain
		tenant.related('domains').save(domain)
		await TenancyController.fireEvents(tenant, validated)
		await tenant.load('domains')

		return tenant
	}

	private static async fireEvents(tenant: Tenant, validated: any) {
		const password = randomString(8, 'Aa#')
		await Event.emit('new:tenant', {
			tenant,
			data: {
				name: `${validated.firstName} ${validated.lastName}`,
				email: validated.email,
				phoneNumber: validated.phoneNumber,
				password,
			},
		})

		console.log('Default credentials are => ', {password, email: validated.email})
	}
}
