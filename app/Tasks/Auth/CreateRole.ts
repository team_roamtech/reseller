import Role from 'App/Models/Tenants/Role'

export default class CreateRole {
  public static async run (connection: string, data: any) {
    return await Role.create(
      data,
      {connection},
    )
  }
}
