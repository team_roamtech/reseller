import {rules, schema} from '@ioc:Adonis/Core/Validator'

export const tenantSignUpSchema = schema.create({
	firstName: schema.string({trim: true}, [rules.minLength(3), rules.maxLength(45)]),
	lastName: schema.string(),
	email: schema.string({}, [rules.email(), rules.maxLength(60)]),
	domain: schema.string({trim: true, escape: true}, [
		rules.alpha({allow: ['dash', 'underscore']}),
		rules.unique({table: 'domains', column: 'domain'}),
		rules.maxLength(32),
	]),
	phoneNumber: schema.string({}, [rules.phone()]),
})
