export const randomString = function (length, kind) {
	let i
	let str = ''
	let opts = kind || 'aA#'
	let possibleChars = ''

	if (kind.indexOf('*') > -1) {
		opts = 'aA#!'
	} // use all possible charsets

	// Collate charset to use
	if (opts.indexOf('#') > -1) {
		possibleChars += '0123456789'
	}
	if (opts.indexOf('a') > -1) {
		possibleChars += 'abcdefghijklmnopqrstuvwxyz'
	}
	if (opts.indexOf('A') > -1) {
		possibleChars += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	}
	if (opts.indexOf('!') > -1) {
		possibleChars += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\'
	}

	for (i = 0; i < length; i++) {
		str += possibleChars.charAt(Math.floor(Math.random() * possibleChars.length))
	}
	return str
}


export const networkMapper = (row, field) => {

	row.network = 'NO_NETWORK';

	let networkSurffixes = [
		{
			network: 'safaricom',
			regex: "^(?:254|0)(7(?:[0129]\\d{7}|5[789]\\d{6}|4[01234568]\\d{6}|6[89]\\d{6})|1(?:1[0-9]\\d{6}))$"
		},
		{
			network: 'airtel',
			regex: "^(?:254|0)(7(?:[38]\\d{7}|5[0123456]\\d{6})|1(?:0[0-9]\\d{6}))$"
		},

		{
			network: 'telkom',
			regex: "^(?:254|0)(7(?:[7]\\d{7})|2(?:0[0-9]\\d{6}))$"
		},

		{
			network: 'equitel',
			regex: "^(?:254|0)7(?:6[345]\\d{6})$"
		},
		{
			network: 'faiba',
			regex: "^(?:254|0)7(?:4[7]\\d{6})$"
		}
	]

	networkSurffixes.forEach( (suffix) => {

		let match = row[field].match(suffix.regex);

		if(match) {
			row.network = suffix.network;
		}

	});

	return row;
}
