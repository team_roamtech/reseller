const mix = require('laravel-mix');
const path = require('path');

const tailwindcss = require('tailwindcss');

mix.setPublicPath('public');

mix
    .js(
        'resources/js/main.ts',
        path.resolve(__dirname, 'public/js'),
    )
    .sass(
        'resources/assets/scss/app.scss',
        path.resolve(__dirname, 'public/css'),
    )
    .webpackConfig({
        context: __dirname,
        node: {
            __filename: true,
            __dirname: true,
        },
        resolve: {
            alias: {
                '@': path.resolve(__dirname, 'resources/js'),
                '~': path.resolve(__dirname, 'resources/js'),
                '@sass': path.resolve(__dirname, 'resources/assets/scss'),
            },
            extensions: ["*", ".js", ".jsx", ".vue", ".ts", ".tsx"]
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: "ts-loader",
                    options: { appendTsSuffixTo: [/\.vue$/] },
                    exclude: /node_modules/,
                },
            ],
        }
    })
    .options({
        processCssUrls: false,
        postCss: [tailwindcss('./tailwind.config.js')]
    })
    .version()
    .vue();
