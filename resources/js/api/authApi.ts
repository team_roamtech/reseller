import axios from 'axios';

export const register = (form) => {
	return axios.post('register', form);
}

export const login = (form) => {
	return axios.post('auth/token', form);
}
export const getUser = () => {
    return axios.get('auth/user');
}
