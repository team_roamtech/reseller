import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/index'
import {DateTime} from "luxon";

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'dashboard',
            component: () => import('../views/dashboard/index.vue'),
        },

        {
            path: '/contacts',
            name: 'contacts',
            component: () => import('../views/contacts/index.vue'),
        },

        {
            path: '/messaging',
            name: 'sms',
            component: () => import('../views/sms/index.vue'),
        },

        {
            path: '/reports',
            name: 'reports',
            component: () => import('../views/reports/index.vue'),
        },

        {
            path: '/settings',
            name: 'settings',
            component: () => import('../views/reports/index.vue'),
        },

        {
            path: '/help',
            name: 'help',
            component: () => import('../views/reports/index.vue'),
        },

        {
            path: '/login',
            name: 'login',
            component: () => import('../views/auth/login.vue'),
            meta: {
                guest: true,
                layout: 'guest',
            },
        },

        {
            path: '/register',
            name: 'register',
            component: () => import('../views/auth/register.vue'),
            meta: {
                guest: true,
                layout: 'guest',
            },
        },

        {
            path: '*',
            component: {template: '<section>Page does not exist</section>'},
        },
    ],
})

router.beforeEach((to, _from, next) => {
    const authenticated = store.getters.auth.token !== null ?
        DateTime.fromISO(store.getters.auth.token.expires_at) > DateTime.now() :
        false;

    if (to.matched.some(record => record.meta.guest && !authenticated)) {
        next()
        return;
    }
    if (to.matched.some(record => record.meta.guest && authenticated)) {
        next({name: 'dashboard'})
        return;
    }
    if (authenticated && !to.matched.some(record => record.meta.guest)) {
        next()
        return;
    } else {
        next({name: 'login'})
    }

})

export default router

