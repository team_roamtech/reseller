import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";

/**
 * Modules
 */
import auth from './modules/auth';
import layout from './modules/layout';

Vue.use(Vuex);

export default new Vuex.Store({

	plugins: [
		createPersistedState({
            paths: ["auth", "layout "]
        })
	],

	modules: {
		auth,
		layout
	}
});
