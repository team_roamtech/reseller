import {register, login, getUser} from "../../../api/authApi";
//import {dummyUser} from "../../../utils/etc";
import router from '../../../router/index';
import {Notification} from "element-ui";
import {DateTime} from "luxon";

const state = ({
    auth: {token: null, user: null},
});

const getters = {
    auth: state => state.auth,
    isAuthenticated: state => state.auth.token !== null ?  DateTime.fromISO(state.auth.token.expires_at) > DateTime.now() : false,
};

const mutations = {
    SET_TOKEN(state, data) {
        state.auth.token = data;
    },
    SET_USER(state, data) {
        state.auth.user = data;
    },
};

const actions = {
    SUBMIT_REGISTER_FORM({commit}, form) {
        register(form)
            .then(({data}) => {
                if (data.status === 200) {
                    commit();
                }
            })
            .catch((resp) => {
                console.log(resp, 'this is the response');
            })
    },

    SUBMIT_LOGIN_FORM({commit}, form) {

        login(form)
            .then((response) => {
                if (response.status === 200) {
                    setUser(commit, response.data);
                }
            })
            .catch((error) => {
                
                if (error.response.status === 401) {
                    Notification({
                        title: 'Login Failed',
                        type: 'warning',
                        message: "The provided credentials were invalid, please try again!",
                    });
                }
            })
    },
};

const setUser = (commit, data) => {
    commit('SET_TOKEN', data)
    getUser().then(({data}) => {
        commit('SET_USER', data);
        router.push({name: 'dashboard'});
    })
}

export default {
    state,
    getters,
    mutations,
    actions,
};
