/**
 * Global Axios
 */
import axios from 'axios';
import store from './store/index'

axios.defaults.withCredentials = true;
axios.defaults.baseURL = `/api/v1`;
axios.interceptors.request.use(config => {
    const token = store.getters.auth.token
    if (token !== null) {
        config.headers['Authorization'] = `Bearer ${token.token}`
    }
    return config
})

/**
 * Plugins
 */
require('./plugins/meta');
require('./plugins/element-ui');
