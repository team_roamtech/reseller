import Route from '@ioc:Adonis/Core/Route'
import Env from '@ioc:Adonis/Core/Env'

Route.get('/', 'Tenants/HomeController.index').as('home')
	.domain(`:tenant.${Env.get('BASE_DOMAIN')}`)
	.middleware('tenancy')

Route.get('*', 'Tenants/HomeController.index')
	.as('not_found')
	.domain(`:tenant.${Env.get('BASE_DOMAIN')}`)
	.middleware('tenancy')
