import Route from '@ioc:Adonis/Core/Route'

Route.group(async () => {
	Route.post('accounts/register', 'Landlord/TenancyController.register')
}).prefix('tenants')
