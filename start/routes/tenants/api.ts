import Route from '@ioc:Adonis/Core/Route'
import Env from '@ioc:Adonis/Core/Env'

Route.group(async () => {

	Route.group(async () => {
        Route.post('token', 'Tenants/Auth/LoginController.login')
            .as('token')
        Route.get('user', 'Tenants/Auth/LoginController.profile')
            .as('user.profile').middleware('auth')
        Route.post('permissions', 'Tenants/Auth/ACLController.createPermission')
            .as('permission.create')
            .middleware('auth')
        Route.get('permissions', 'Tenants/Auth/ACLController.getAllPermissions')
            .as('permission.list')
            .middleware('auth')
    }).prefix('auth').as('auth')

	Route.post('app-meta-data', 'Tenants/Settings/MetaDataController.store').as('app.meta')
	Route.post('app-layout', 'Tenants/Settings/LayoutController.store').as('app.layout')
	Route.post('app-media', 'Tenants/Settings/AppMediaController.store').as('app.media')

    Route.post('contactgroups', 'Tenants/ContactGroupsController.store').as('contacts.create')
    Route.post('contactgroups/:id', 'Tenants/ContactGroupsController.retreive').as('contacts.retreive')
    Route.post('contactgroups/edit/:id', 'Tenants/ContactGroupsController.editContactGroup').as('contacts.edit_contact_group')
    Route.delete('contactgroups/delete/:id', 'Tenants/ContactGroupsController.deleteContactGroup').as('contacts.delete_contact_group')
    Route.post('contactgroups/:id/contact/:_id', 'Tenants/ContactGroupsController.editContact').as('contacts.edit_contact')
    Route.delete('contactgroups/:id/contact/:_id', 'Tenants/ContactGroupsController.deleteContact').as('contacts.delete_contact')
    Route.post('contactgroups/:id/addcontact', 'Tenants/ContactGroupsController.addContact').as('contacts.add_contact')
   
    
})
  .domain(`:tenant.${Env.get('BASE_DOMAIN')}`)
  .prefix('api/v1').as('api.v1')
  .middleware('tenancy')


